const api = require('./Api');

async function createGame() {

  var requestOptions = {
    method: 'POST',
    redirect: 'follow',
    credentials: "include",
  };

  let response = await fetch(api.api_url + "/api/game", requestOptions);

  return await response.json();


}

async function getGames() {

  var requestOptions = {
    method: 'GET',
    redirect: 'follow',
    credentials: "include"
  };

  let response =  await fetch(api.api_url + "/api/game", requestOptions);

  return await response.json();


}

async function deleteGame(gameId) {

  var requestOptions = {
    method: 'DELETE',
    redirect: 'follow',
    credentials: "include"
  };
 
  await fetch(api.api_url + `/api/game/${gameId}`, requestOptions);



}

export default {
  createGame,
  deleteGame,
  getGames
};