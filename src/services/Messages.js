const api = require('./Api');

async function getMessages(room) {

  var requestOptions = {
    method: 'GET',
    redirect: 'follow',
    credentials: "include"
  };

  let response =  await fetch(api.api_url +  `/api/messages/${room}`, requestOptions);

  return response.json();
}

export default {
    getMessages
};