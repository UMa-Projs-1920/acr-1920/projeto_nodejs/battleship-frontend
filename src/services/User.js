const api = require('./Api');

async function getUser() {

  var requestOptions = {
    method: 'GET',
    redirect: 'follow',
    credentials: "include"
  };

  let response =  await fetch(api.api_url +  "/api/user", requestOptions);

  return response.json();


}


async function getRanking() {

  var requestOptions = {
    method: 'GET',
    redirect: 'follow',
    credentials: "include"
  };

  let response =  await fetch(api.api_url + "/api/ranking", requestOptions);

  return response.json();


}

async function getAuth() {

  var requestOptions = {
    method: 'GET',
    redirect: 'follow',
    credentials: "include"
  };

  let response =  await fetch(api.api_url + "/api/auth", requestOptions);

  return response.json();


}

async function setPassword(newPassword, oldPassword) {
  var requestOptions = {
    method: 'POST',
    redirect: 'follow',
    credentials: "include",
    headers: {
      'Accept' : 'application/json',
      'Content-Type' : 'application/json'
    },
    body: JSON.stringify({
      newPassword,
      oldPassword
    })
  };

  let response =  await fetch(api.api_url + "/api/user/password", requestOptions);

  return response.json();
}

export default {
  getUser,
  getRanking,
  setPassword,
  getAuth
};