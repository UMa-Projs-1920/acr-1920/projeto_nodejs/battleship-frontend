import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Profile from '../views/Profile.vue'
import Game from '../views/Game.vue'
import Ranking from '../views/Ranking.vue'
import NotAuthorized from '../views/NotAuthorized.vue'
import UserService from '../services/User'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/profile',
    name: 'profile',
    component: Profile
  },
  {
    path: '/ranking',
    name: 'ranking',
    component: Ranking
  },
  {
    path: '/error',
    name: 'error',
    component: NotAuthorized
  },
  {
    path: '/play/:gameId',
    name: 'play',
    component: Game
  },
  {
    path: '*',
    component: () => {}
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  base : '/game',
  mode: 'history',
  routes
})

router.beforeEach(async (to, from, next) => {



    let result = await UserService.getAuth();

    if(result && result.auth) {
      next();
    } else {
      window.location.replace("/login");
    }
  
})

export default router
